using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public InteractionType Type;

    public enum InteractionType
    {
        ON_TRIGGER,
        MANUAL,
        COLLISION
    }

    public virtual void Interact(GameObject interactor)
    {

    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {

    }
    public virtual void OnTriggerStay2D(Collider2D collision)
    {

    }
}
