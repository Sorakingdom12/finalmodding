using UnityEngine;

public class SpikesInteraction : Interactable
{
    [SerializeField] Transform _RespawnPoint;

    public override void Interact(GameObject interactor)
    {
        interactor.GetComponent<PlayerController>().TakeDamage(1);
        interactor.transform.position = _RespawnPoint.position;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            Interact(collision.gameObject);
    }
}
