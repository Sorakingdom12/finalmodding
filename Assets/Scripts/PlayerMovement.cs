using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("References")]
    [SerializeField] PlayerController _Controller;
    [SerializeField] Animator _Animator;

    [Header("Variables")]
    public float RunSpeed;

    float _HorizontalMove;

    bool _Jump = false;
    bool _Crouch = false;
    private void Update()
    {
        _HorizontalMove = Input.GetAxisRaw("Horizontal") * RunSpeed;
        _Animator.SetFloat("Speed", Mathf.Abs(_HorizontalMove));
        if (Input.GetButtonDown("Jump"))
        {
            _Jump = true;
            _Animator.SetBool("IsJumping", true);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
            _Crouch = true;
        else if (Input.GetKeyUp(KeyCode.LeftControl))
            _Crouch = false;
    }

    public void OnLanding()
    {
        _Animator.SetBool("IsJumping", false);
    }

    public void OnCrouching(bool isCrouching)
    {
        _Animator.SetBool("IsCrouching", isCrouching);
    }
    private void FixedUpdate()
    {
        _Controller.Move(_HorizontalMove * Time.fixedDeltaTime, _Crouch, _Jump);
        _Jump = false;
    }
}
