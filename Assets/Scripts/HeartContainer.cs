﻿using UnityEngine;

public class HeartContainer : MonoBehaviour
{
    public GameObject Heart;

    public enum State
    {
        ACTIVE,
        LOST
    }

    public void UpdateState(State state)
    {
        if (state == State.ACTIVE) Heart.SetActive(true);
        else Heart.SetActive(false);
    }
}