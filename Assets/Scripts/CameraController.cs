using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform _Player;



    // Update is called once per frame
    void Update()
    {
        transform.position = _Player.position;
    }
}
