using System.Collections.Generic;
using UnityEngine;

public class BossFightController : MonoBehaviour
{
    [SerializeField] GameObject Boss;
    [SerializeField] List<GameObject> Doors;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            foreach (GameObject door in Doors)
            {
                door.SetActive(!door.activeSelf);
            }
            Boss.SetActive(!Boss.activeSelf);
        }
    }
}
