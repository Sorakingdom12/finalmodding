using UnityEngine;

public class PowerSourceInteractable : Interactable
{
    [SerializeField] Transform CityPosition;

    public override void Interact(GameObject interactor)
    {
        interactor.GetComponent<PlayerController>().SetPower("DoubleJump");
        transform.position = CityPosition.position;
        enabled = false;
    }

    private void FixedUpdate()
    {
        Collider2D collision = Physics2D.OverlapBox(transform.position, new Vector2(.35f, .35f), 0);
        if (collision != null && collision.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
                Interact(collision.gameObject);
        }
    }

}
